﻿namespace encuesta
{
    partial class encuesta2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.res1 = new System.Windows.Forms.RadioButton();
            this.res2 = new System.Windows.Forms.RadioButton();
            this.res3 = new System.Windows.Forms.RadioButton();
            this.res4 = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(128, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cual es la Capital de Alemania?";
            // 
            // res1
            // 
            this.res1.AutoSize = true;
            this.res1.Location = new System.Drawing.Point(152, 74);
            this.res1.Name = "res1";
            this.res1.Size = new System.Drawing.Size(51, 17);
            this.res1.TabIndex = 1;
            this.res1.TabStop = true;
            this.res1.Text = "Berlin";
            this.res1.UseVisualStyleBackColor = true;
            // 
            // res2
            // 
            this.res2.AutoSize = true;
            this.res2.Location = new System.Drawing.Point(152, 97);
            this.res2.Name = "res2";
            this.res2.Size = new System.Drawing.Size(82, 17);
            this.res2.TabIndex = 2;
            this.res2.TabStop = true;
            this.res2.Text = "Washintong";
            this.res2.UseVisualStyleBackColor = true;
            // 
            // res3
            // 
            this.res3.AutoSize = true;
            this.res3.Location = new System.Drawing.Point(152, 120);
            this.res3.Name = "res3";
            this.res3.Size = new System.Drawing.Size(64, 17);
            this.res3.TabIndex = 3;
            this.res3.TabStop = true;
            this.res3.Text = "Napoles";
            this.res3.UseVisualStyleBackColor = true;
            // 
            // res4
            // 
            this.res4.AutoSize = true;
            this.res4.Location = new System.Drawing.Point(152, 143);
            this.res4.Name = "res4";
            this.res4.Size = new System.Drawing.Size(84, 17);
            this.res4.TabIndex = 4;
            this.res4.TabStop = true;
            this.res4.Text = "Pearl Harbor";
            this.res4.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(152, 215);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 44);
            this.button1.TabIndex = 5;
            this.button1.Text = "Siguiente Pregunta";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // encuesta2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 415);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.res4);
            this.Controls.Add(this.res3);
            this.Controls.Add(this.res2);
            this.Controls.Add(this.res1);
            this.Controls.Add(this.label1);
            this.Name = "encuesta2";
            this.Text = "encuesta2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton res1;
        private System.Windows.Forms.RadioButton res2;
        private System.Windows.Forms.RadioButton res3;
        private System.Windows.Forms.RadioButton res4;
        private System.Windows.Forms.Button button1;
    }
}